import React, { Component } from 'react'
import {Text,TouchableOpacity,Button} from 'react-native'

export default class Main extends Component{

    render(){
        return(
        <TouchableOpacity
        onPress={()=> {this.props.navigation.navigate('QuizPage')}}>
            <Text>
                Start
            </Text>
        </TouchableOpacity>
        )
    }
}