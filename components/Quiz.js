import React, { Component } from 'react';
import {
    Alert,
    FlatList,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View } from 'react-native';
import Question from './Question';
import QuestionModel from './QuestionModel';

export default class Quiz extends Component{
    /** Basic constructor. */
    constructor(props){
        super(props);
    }

    /** First tallies up the scores by initializing an array of zeroes with
     *  the indices corresponding to the houses in the following way:
     *    0: Slytherin
     *    1: Gryffindor
     *    2: Ravenclaw
     *    3: Hufflepuff
     *  Then it checks to see which index is the highest, and then uses that information
     *  to choose the corresponding house and pass it to the results Navigation page.
     */
    _calculateScore = () => {
        // Tally scores
        let scores = [0, 0, 0, 0]
        for(let i = 0; i < 5; i++){
            let answer = this.questions[i].getSelected();
            if(answer != 'none'){
                if(answer === 'choice1') scores[0] += 1;
                else if(answer === 'choice2') scores[1] += 1;
                else if(answer === 'choice3') scores[2] += 1;
                else if(answer === 'choice4') scores[3] += 1;
            }
            else{
                return Alert.alert(
                    'Slow down.',
                    'You\'ve still got some unanswered questions.'
                )
            }
        }
        
        // Choose winner
        winningScore = scores.indexOf(Math.max(scores));
        let winningHouse = 'none';
        switch(winningScore){
            case 0:
                winningHouse = "Slytherin";
                break;
            case 1:
                winningHouse = "Gryffindor";
                break;
            case 2:
                winningHouse = "Ravenclaw";
                break;
            case 3:
                winningHosue = "Hufflepuff";
                break;
            default:
                break;
        }

        // Pass winner to ResultsPage
        this.props.navigation.navigate('ResultsPage', {winner: winningScore});
    }

    /** Simple function that turns each hardcoded QuestionModel into a Question Component. */
    _mapQuestionModelsToQuestions = () =>{
        for(let i = 0; i < this.questionModels.length; i++){
            this.questions.push(<Question
                choice1={this.questionModels[i].choice1}
                choice2={this.questionModels[i].choice2}
                choice3={this.questionModels[i].choice3}
                choice4={this.questionModels[i].choice4}
                text={this.questionModels[i].text}
                key={i}
            />)
        }
    }

    /** Simply calls the render function of the Component item passed to it. */
    _renderQuestion = ({item}) => {
        return (item.render());
    }

    /** This is the array that will hold our Question components. */
    questions=[]

    /** This array holds the list of hardcoded QuestionModels with the Question Text, Choices, and ID. */
    questionModels=[
        new QuestionModel(
            "Those closest to you would describe you as:",
            "Super evil.",
            "Really brave.",
            "Wicked smart.",
            "Exceptionally huggable.",
            "1"
        ),
        new QuestionModel(
            "You're walking down the street and you see a homeless person begging for change, do you:",
            "Punch him and steal his cardboard box; handouts are for weaklings.",
            "Bravely tell him to get a job.",
            "Offer him a full-ride scholarship to a university so he can broaden his horizons.",
            "Give him a hug.",
            "2"
        ),
        new QuestionModel(
            "Your friend calls you saying he's in trouble and he needs help hiding a dead body, do you:",
            "Kill your friend and hide both bodies.",
            "Bravely report him to the police.",
            "Go to the library for information on hiding dead bodies.",
            "Hug the dead body.",
            "3"
        ),
        new QuestionModel(
            "What is your favorite way to de-stress after a long day?",
            "Rob a liquor store.",
            "Bravely rescue kittens stuck in trees.",
            "Read encyclopedias.",
            "Hug my pet badger.",
            "4"
        ),
        new QuestionModel(
            "Dr. Mano tells you to give him $5 or he'll give you an F. Do you:",
            "Record the conversation and threaten to report him unless he gives you an A.",
            "Bravely report him to Dr. Xi.",
            "Give him the $5; grades are more important than morals.",
            "Give him a hug (he probably needs it).",
            "5"
        )
    ]

    /** Set to render the TouchableOpacity that is the 'Submit Quiz' button, and then
     * the ScrollView that renders all of the questions.
     */
    render(){
        this._mapQuestionModelsToQuestions();
        return(
            <View>
                <TouchableOpacity onPress={() => this._calculateScore()}>
                    <Text>results</Text>
                </TouchableOpacity>
                <ScrollView
                    contentContainerStyle={styles.scrollContent}
                    style={styles.scroll}
                >
                    {this.questions}
                </ScrollView>
            </View>
        );
    }   
}

const styles = StyleSheet.create({
    scroll: {
      alignSelf: 'stretch'
    },
    scrollContent: {
      alignItems: 'center'
    },
  });