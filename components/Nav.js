import React, { Component } from 'react'
import {StackNavigator} from 'react-navigation'
import HomePage from './Main'
import Quiz from './Quiz'
import Results from './Results'

const Root = StackNavigator({
    HomePage:{
        screen: HomePage,
    } ,
    QuizPage:{
        screen: Quiz,
    },
    ResultsPage:{
        screen: Results
    },
});

export default class Nav extends Component{

    render(){
        return <Root/>
    }

}