//import { Component } from 'react-native';

import React, { Component } from 'react';
import {
  Container, 
  Content, 
  Header,
  Left,
  ListItem, 
  Text,
  Radio,
  Right 
} from 'native-base';
import { TouchableOpacity, View } from 'react-native';

export default class Question extends Component{
  constructor(props){
    super(props);
    this.state = {
      itemSelected: 'none'
    }
  }

  getSelected(){
    return this.state.itemSelected;
  }

  render() {
    return (
      <View>
        <Text>Question {this.props.id}: {this.props.text}</Text>
        <ListItem>
          <TouchableOpacity 
            style={{flexDirection: 'row'}}
            onPress={() => this.setState({ itemSelected: 'choice1'})}>
            <Text>{this.props.choice1}</Text>
            <Right>
              <Radio selected={this.state.itemSelected == 'choice1'}/>
            </Right>
          </TouchableOpacity>
        </ListItem>
        <ListItem>
          <TouchableOpacity 
            style={{flexDirection: 'row'}}
            onPress={() => this.setState({ itemSelected: 'choice2'})}>
            <Text>{this.props.choice2}</Text>
            <Right>
              <Radio selected={this.state.itemSelected == 'choice2'}
                 />
            </Right>
          </TouchableOpacity>
        </ListItem>
        <ListItem>
          <TouchableOpacity 
              style={{flexDirection: 'row'}}
              onPress={() => this.setState({ itemSelected: 'choice3'})}>
            <Text>{this.props.choice3}</Text>
            <Right>
              <Radio selected={this.state.itemSelected == 'choice3'} />
            </Right>
          </TouchableOpacity>
        </ListItem>
        <ListItem>
          <TouchableOpacity 
              style={{flexDirection: 'row'}}
              onPress={() => this.setState({ itemSelected: 'choice4'})}>    
            <Text>{this.props.choice4}</Text>
            <Right>
              <Radio selected={this.state.itemSelected == 'choice4'}/>
            </Right>
          </TouchableOpacity>
        </ListItem>
      </View>
    );
  }
}