import React, { Component } from 'react'
import {Text,TouchableOpacity,View} from 'react-native'

export default class Results extends Component{

    render(){
        return(
            <View>
                <Text>{this.props.navigation.state.params.winner}</Text>
                <TouchableOpacity
                    onPress = { ()=> {this.props.navigation.navigate('HomePage')}}>
                    <Text>take quiz again</Text>
                </TouchableOpacity>
            </View>
        )
    }
}