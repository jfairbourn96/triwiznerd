export default class QuestionModel{
  /** 
   * Each Question will have the following data:
   * choice1: the Slytherin choice
   * choice2: the Gryffindor choice
   * choice3: the Ravenclaw choice
   * choice4: the Hufflepuff choice
   * text: the actual text of the question
   * id: the unique id of the question
   */
  constructor(text, choice1, choice2, choice3, choice4, id){
    this.text = text;
    this.choice1 = choice1;
    this.choice2 = choice2;
    this.choice3 = choice3;
    this.choice4 = choice4;
    this.id = id;
  }
}